package com.example.dubaiscala.spark;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Sparktest implements scala.Serializable {
	private static final Logger log = LoggerFactory.getLogger(Sparktest.class);

	public void run() {
		executeSparkJob1();
	}

	public void executeSparkJob1() {
		System.out.println("Spark Started");
		SparkSession spark = SparkSession.builder().appName("ETLWithSpark").master("spark://172.24.120.123:7077")
				.config("spark.es.nodes", "http://172.24.120.123").config("spark.es.port", "9200")
//				.config("spark.eventLog.enabled", "true")
//			    .config("spark.eventLog.dir", "hdfs://172.24.120.123:9000/user/spark-events")
//			    .config("spark.history.provider", "org.apache.spark.deploy.history.FsHistoryProvider") 
//			    .config("spark.history.fs.logDirectory", "hdfs://172.24.120.123:9000/user/spark-events-1")
				.config("spark.es.net.http.auth.user", "elastic").config("spark.es.net.http.auth.pass", "123456")
				.config("spark.es.net.ssl", "false").getOrCreate();

		try {
			String hdfsPath = "hdfs://172.24.120.123:9000/user/customerDetails50L.csv";

			Dataset<Row> sourceDF = spark.read().option("header", "true").csv(hdfsPath).cache();

			Dataset<Row> filteredDeviceName = sourceDF.filter(sourceDF.col("FirstName").equalTo("John"));

//			sourceDF.write().mode("overwrite").parquet("/home/env/excel/destination.parquet");
			sourceDF.write().mode("overwrite").parquet("/home/hadoop/excel/destination.parquet");

			Dataset<Row> parquetDataFrame = spark.read().parquet("/home/hadoop/excel/destination.parquet");

			parquetDataFrame.write().format("org.elasticsearch.spark.sql")
					.option("es.nodes", "http://172.24.120.123:9200").option("es.port", "9200")
					.option("es.resource", "50lackrecord").option("es.net.ssl", "false")
					.option("es.nodes.wan.only", "true").mode("overwrite").save();

			filteredDeviceName.write().format("org.elasticsearch.spark.sql")
					.option("es.nodes", "http://172.24.120.123:9200").option("es.port", "9200")
					.option("es.resource", "withfilter").option("es.net.ssl", "false")
					.option("es.nodes.wan.only", "true").mode("overwrite").save();

			System.out.println("Spark completed");

		} catch (Exception e) {
			System.err.println("Error occurred: " + e.getMessage());
		} finally {
			spark.stop();
		}
	}
}
