package com.example.dubaiscala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.example.dubaiscala.spark.Sparktest;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan({"com.example"})
public class DubaiPolicawithscalaApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DubaiPolicawithscalaApplication.class, args);
        Sparktest sparkTest = context.getBean(Sparktest.class);
        sparkTest.run();
    }
}
